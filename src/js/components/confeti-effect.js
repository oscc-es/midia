class ConfetiPiece {
  constructor(x, y, ctx) {
    this.COLORS = [
      '#7ffdd2',
      '#7fb1fe',
      '#05fcfd',
      '#fde2c3',
      '#fec0cb',
      '#68a3fe',
      '#fdfdfd',
      '#23fc04',
      '#73e0fe',
      '#fff',
      '#feb846',
      '#fdda05',
      '#b2253c',
      '#51c483'
    ];
    this.stop = false;
    this.ctx = ctx;
    this.gravity = 0.098;
    this.x = this.initialX = x;
    this.y = this.initialY = y;
    this.unit = Math.min(x, y) * 0.4;

    this.randomInitialise();
  }

  stopRandomInitialise() {
    this.stop = true;
  }

  randomInitialise() {
    this.x = this.initialX;
    this.y = this.initialY;
    this.opacity = 1;

    this.vx = -5 + Math.random() * 10;
    this.vy = Math.random() * (-Math.sqrt(25 - this.vx * this.vx) - 5);

    this.width = (Math.random() * this.unit) / 20;
    this.heigth = Math.random() * this.width;

    this.heigthv = Math.random() * 3;

    this.rotation = Math.random() * 360;
    this.rotationv = Math.random() * 2;

    this.color = this.COLORS[Math.floor(Math.random() * this.COLORS.length)];

    this.speedOpacity = Math.random() * 0.01;
  }

  update() {
    if (this.opacity < 0) {
      this.randomInitialise();
    }

    if (this.opacity >= 0) {
      this.opacity -= this.speedOpacity;
    }

    this.heigthv =
      this.heigth < this.width && this.heigth > 0 ? this.heigthv : -this.heigthv;
    this.heigth += this.heigthv;
    this.rotation += this.rotationv;
    this.x = this.x + this.vx;
    this.y = this.y + this.vy;
    this.vy += this.gravity;
  }

  draw() {
    this.ctx.save();

    this.ctx.beginPath();
    this.ctx.globalAlpha = this.opacity;
    this.ctx.fillStyle = this.color;
    this.ctx.translate(this.x, this.y);
    this.ctx.rotate((this.rotation * Math.PI) / 180);
    this.ctx.ellipse(
      0 - this.width / 2,
      0 - this.heigth / 2,
      Math.abs(this.width),
      Math.abs(this.heigth),
      0,
      0,
      2 * Math.PI
    );
    this.ctx.fill();
    this.ctx.closePath();

    this.ctx.restore();
  }
}

class ConfetiEffect extends HTMLElement {
  constructor() {
    super();

    this.duration = 5 * 60;
    this.counter = 0;
    this.amount = 500;
    this.confeti = [];
  }

  connectedCallback() {
    this.innerHTML = '<canvas></canvas>';
    this.canvas = this.querySelector('canvas');
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.ctx = this.canvas.getContext('2d');

    this.loopId = requestAnimationFrame(timeStamp => this.animationLoop(timeStamp));
  }

  stopAnimation() {
    if (this.loopId) {
      cancelAnimationFrame(this.loopId);
    }
  }

  animationLoop(timeStamp) {
    // 1 - Clear & resize
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.ctx = this.canvas.getContext('2d');
    // 2 Draw & Move
    this.drawScene();
    this.counter++;
    // call again mainloop after 16.6ms
    //(corresponds to 60 frames/second)
    if (this.confeti.length && this.counter < this.duration) {
      this.loopId = requestAnimationFrame(timeStamp => this.animationLoop(timeStamp));
    } else {
      cancelAnimationFrame(this.loopId);
    }
  }

  drawScene() {
    if (this.confeti.length < this.amount - 1 && this.counter < 60) {
      for (let i = 0; i <= 50 - 1; i++) {
        this.confeti.push(
          new ConfetiPiece(this.canvas.width / 2, this.canvas.height / 2, this.ctx)
        );
      }
    }

    for (let i = 0; i <= this.confeti.length - 1; i++) {
      if (this.confeti[i].opacity < 0) {
        this.confeti.splice(i, 1);
        continue;
      }

      this.confeti[i].draw();
      this.confeti[i].update();
    }
  }
}

customElements.define('confeti-effect', ConfetiEffect);
