import Content from './components/content.js';

class App {
  constructor() {
    this.contentInstance = new Content();
    this.myDayInstances = document.querySelectorAll('[data-element="myDay"]');

    this.confetiEffect = document.createElement('confeti-effect');

    // Run the initial content application and the listener
    this.applyContent();
    this.cleanLocalStorage();
    this.listen();
  }

  listen() {
    // Listens for checkboxes changes and applies to local storage accordingly
    this.myDayInstances.forEach(myDay => {
      myDay.addEventListener('change', () => {
        this.contentInstance.save(myDay.getAttribute('data-id'), myDay.checked);

        let areAllChecked = myDay.checked;
        if (areAllChecked) {
          this.myDayInstances.forEach(
            myDay => (areAllChecked = myDay.checked && areAllChecked)
          );
        }

        if (areAllChecked) {
          document.body.appendChild(this.confetiEffect);
        }

        if (!areAllChecked && document.querySelector('confeti-effect')) {
          document.body.removeChild(this.confetiEffect);
          this.confetiEffect = document.createElement('confeti-effect');
        }
      });
    });

    // If content changes in another tab, sync it up
    window.addEventListener('storage', evt => {
      const {isTrusted} = evt;

      if (!isTrusted) {
        return;
      }

      this.applyContent();
    });
  }

  // Loads the content from the content interface and applies it
  applyContent() {
    this.existingContent = [];

    this.myDayInstances.forEach(myDay => {
      const id = myDay.getAttribute('data-id');
      myDay.checked = this.contentInstance.load(id);
      this.existingContent.push(id);
    });
  }

  cleanLocalStorage() {
    this.contentInstance.list().forEach(item => {
      if (!this.existingContent.includes(item)) {
        this.contentInstance.delete(item);
      }
    });
  }
}

const appInstance = new App();
