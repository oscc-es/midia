---
title: 'Mi día'
metaDesc: 'Mi día es una simple PWA (página web progresiva) que busca motivar la constancia sobre una serie de hábitos predefinidos que se pueden marcar como realizados a lo largo de cada día.'
layout: 'layouts/home.njk'
permalink: /
---

<p>
  Un satisfactorio botoncito en verde por cada actividad que realice hoy entre esta lista de rutinas que quiero fijar en <strong><em>mi día</em></strong>.
</p>
