---
title: 'Información'
layout: layouts/page.njk
metaDesc: '¿Qué es Mí día? ¿qué ha motivado este desarrollo? ¿qué lo ha inspirado? descubre la información relativa a este proyecto desarrollado por Óscar García.'
---

[Mi día](/) es una sencilla [<abbr lang="en" title="Progressive web aplication">PWA</abbr>](https://es.wikipedia.org/wiki/Aplicaci%C3%B3n_web_progresiva) [desarrollada por Óscar García](https://www.ogarcia.es) que permite marcar las actividades realizadas durante tu día entre una lista predeterminada de rutinas.

## Motivación de _Mi día_

Encontramos en una tienda un tablero de madera enfocado a niños para marcar tareas básicas del día a día. Automáticamente pensamos que fácilmente se podría convertir un pequeño aporte en un contexto de salud mental. Pasado un tiempo, y viendo que se acercaba una fecha señalada, no me pude resistir a desarrollar esta versión web.

Esta web no recoge datos de ningún tipo. Utiliza el propio dispositivo del usuario para almacenar las tareas marcadas.

## Próximos pasos

La web se ha creado con una persona determinada en la cabeza. Inevitablemente, lo primero que se me pasa por la cabeza es que cualquiera pueda personalizar su tablero de forma sencilla. Hasta que llegue ese momento, si llega, si te apetece tener tu versión personalizada pero no sabes como hacerlo, puedes contactarme a ver si tengo un poquito de tiempo para hacerte el tuyo.

Otras ideas que han rondado son:

- Acceso directo desde icono de la PWA para resetear todas las tareas.
- Permitir que cualquier usuario pueda montar y personalizar su propio tablero.
- Dos tipos de tareas: diarias y semanales. Se implementó pero el 'cliente' final lo descartó.
- Botón de reset para desmarcar todas las tareas. Se implementó pero el 'cliente' final lo descartó.

Puedes echar un ojo al código fuente en el [repositorio de Mi día](https://gitlab.com/oscc-es/midia) y avisar de errores o sugerencias.

## Créditos

Esta web está desarrollada utilizando [Eleventy](https://www.11ty.dev/), un generador de sitios estáticos creado y mantenido por [Zach Leatherman](https://fediverse.zachleat.com/@zachleat). Tengo ya tan interiorizado su uso que, injustamente, no lo menciono tanto como debería.

La mayoría de ilustraciones son modificaciones de diseños de [vijay verma](https://vijayverma.co).

Los botones para marcar las tareas salieron prácticamente tal cual están de [una de las ediciones de <i lang="en">“Front-End Challenges Club”</i>](https://piccalil.li/tutorial/solution-002-toggle-switch/) de [Andy Bell](https://mastodon.social/@belldotbz).

El 'huevo de pascua' de la aplicación es mi adaptación en componente web de [este codepen](https://codepen.io/FabioG/pen/PPVPyd) de [Fabio García](https://codepen.io/FabioG/).

Gracias infinitas al diseñador y la cabeza pensante de aquel objeto de madera de una conocida tienda danesa.

## Si te gusta _Mi día_

Si visitas a menudo esta página, te gusta o te parece útil, considera hacérmelo saber mediante un e-mail a [hola@ogarcia.es](mailto:hola@ogarcia.es) o un mensaje en [mi Mastodon](https://paquita.masto.host/@ogarcia). Siempre motiva saber que tu trabajo llega a alguien.

Si quieres contribuir a que tenga algo más de tiempo para mejorar esta web o desarrollar otras nuevas, puedes [invitarme a un café](https://ko-fi.com/ogarcia).
