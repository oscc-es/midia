const Origin = require('./Origin');
const EleventyFetch = require('@11ty/eleventy-fetch');

class Rtve extends Origin {
  constructor(data) {
    data.type = 'json';
    super(data);

    this.feedUrl = `${this.feedUrl}?lastDays=${Math.ceil(this.feedPeriod)}`;
    this.omdbApiUrl = `http://www.omdbapi.com/?apikey=ed0e3db8&i=`;
  }

  async parseFeedData(response) {
    this.data = response;

    const entries = this.data.page.items.filter(item => {
      let isAvailable = true;

      if (item.expirationDate != null) {
        const dateString = item.expirationDate;
        const dayHour = dateString.split(' ');
        const date = `${dayHour[0].split('-').reverse().join('-')} ${dayHour[1]}`;

        isAvailable = new Date(date).getTime() > new Date().getTime();
      }

      return item.type !== null && item.type.name == 'Completo' && isAvailable;
    });

    if (entries.length < 1) {
      return [];
    }

    this.siteTitle = entries[0].programInfo.title;

    return [
      ...new Set(
        await Promise.all(
          entries.map(async entry => ({
            siteTitle: this.siteTitle,
            siteUrl: this.siteUrl,
            feedUrl: this.feedUrl,
            title: entry.shortTitle,
            url: entry.htmlShortUrl,
            date: new Date(entry.publicationDateTimestamp),
            thumbnail:
              this.cover && entry.previews.vertical
                ? entry.previews.vertical
                : entry.thumbnail,
            cover: this.cover && entry.previews.vertical,
            quality: this.quality ? await this.getEntryQuality(entry) : 0
          }))
        )
      )
    ];
  }

  async getEntryQuality(entry) {
    if (!entry.idImdb) {
      return 0;
    }

    const imdbRating = await this.getImdbRating(entry.idImdb);

    if (!imdbRating || isNaN(imdbRating)) {
      return 0;
    }

    return imdbRating;
  }

  getImdbRating(imdbId) {
    return this.getOmdbData(imdbId).then(response =>
      response.imdbVotes && response.imdbVotes.replaceAll(',', '') > 1000
        ? response.imdbRating
        : 0
    );
  }

  getOmdbData(id) {
    return EleventyFetch(this.omdbApiUrl + id, {
      duration: '1w', // save for 1 week
      type: 'json'
    }).catch(() => {
      return Promise.resolve({imdbRating: 0});
    });
  }
}

module.exports = Rtve;
