const Origin = require('./Origin');
const EleventyFetch = require('@11ty/eleventy-fetch');

class Eitb extends Origin {
  constructor(data) {
    data.type = 'json';
    super(data);
    //mam.eitb.eus/mam/REST/ServiceMultiweb/Playlist2/MULTIWEBTV/
    https: this.omdbApiUrl = `http://www.omdbapi.com/?apikey=${process.env.OMDB_API_KEY}&i=`;
  }

  async parseFeedData(response) {
    this.data = response;

    const entries = this.data.web_media;

    if (entries.length < 1) {
      return [];
    }

    this.siteTitle = entries.name;

    return [
      ...new Set(
        await Promise.all(
          entries.map(async entry => ({
            siteTitle: this.siteTitle,
            siteUrl: this.siteUrl,
            feedUrl: this.feedUrl,
            title: entry.CHAPTER_ES,
            url: entry.htmlShortUrl,
            date: new Date(entry.publicationDateTimestamp),
            thumbnail:
              this.cover && entry.previews.vertical
                ? entry.previews.vertical
                : entry.thumbnail,
            cover: this.cover && entry.previews.vertical,
            quality: this.quality ? await this.getEntryQuality(entry) : 0
          }))
        )
      )
    ];
  }

  async getEntryQuality(entry) {
    if (!entry.idImdb) {
      return 0;
    }

    const imdbRating = await this.getImdbRating(entry.idImdb);

    if (!imdbRating || isNaN(imdbRating)) {
      return 0;
    }

    if (imdbRating > 7.7) {
      return 2;
    }

    if (imdbRating > 7) {
      return 1;
    }

    return 0;
  }

  getImdbRating(imdbId) {
    return this.getOmdbData(imdbId).then(response =>
      response.imdbVotes && response.imdbVotes.replaceAll(',', '') > 1000
        ? response.imdbRating
        : 0
    );
  }

  getOmdbData(id) {
    return EleventyFetch(this.omdbApiUrl + id, {
      duration: '1w', // save for 1 week
      type: 'json'
    }).catch(() => {
      return Promise.resolve({imdbRating: 0});
    });
  }
}

module.exports = Eitb;
