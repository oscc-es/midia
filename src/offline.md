---
title: 'Offline'
layout: layouts/page.njk
permalink: offline.html
eleventyExcludeFromCollections: true
---

Parece que no tiene conexión. Por favor, [vuelva a la página de inicio](/).
