# Mi día

Simple motivational task board on top of my [oscc-base-no-blog 11ty template](https://gitlab.com/oscc-es/oscc-base-no-blog). Check it out at [**midia.ogarcia.es**](https://midia.ogarcia.es).

## Credits

Illustrations based on [vijay verma](https://vijayverma.co) designs.

Switch buttons thanks to [Andy Bell](https://mastodon.social/@belldotbz)'s [_“Front-End Challenges Club”_](https://piccalil.li/tutorial/solution-002-toggle-switch/).

Ester egg's code is my web component version of this [codepen](https://codepen.io/FabioG/pen/PPVPyd) from [Fabio García](https://codepen.io/FabioG/).

## Tasks configuration

Activities list are defined at `src/data/activities.json`.

## Getting started

- Run `npm install`
- Run `npm start` to run locally
- Run `npm run production` to do a prod build
- Run `npm run release` to generate/update the changelog file and increment the corresponding npm version on package.json
